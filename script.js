function showImage() {
    let i = 0;
    let working = true;
    let btnStop = document.createElement("button");
    btnStop.textContent = "Припинити";
    btnStop.addEventListener("click", () => {
        working = false;
    });
    let btnStart = document.createElement("button");
    btnStart.textContent = "Відновити показ";
    btnStart.addEventListener("click", () => {
        working = true;
    });
    document.body.append(btnStart);
    document.body.append(btnStop);
    setInterval(function () {
        if (working) {
            document.querySelectorAll(".image-to-show").forEach((elem) => {
                elem.style.display = "none"
            });
            document.querySelectorAll(".image-to-show").item(i).style.display = "block";
            if (i === 3) {
                i = 0;
            } else {
                i++;
            }
        }
    }, 3000);


}
document.querySelectorAll(".image-to-show").forEach((elem) => {
    elem.style.display = "none"
});
showImage();